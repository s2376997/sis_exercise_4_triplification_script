import sys
import pandas as pd
from rdflib import Graph, Literal, RDF, URIRef, Namespace
from rdflib.namespace import XSD

# Check if the correct number of arguments are provided
if len(sys.argv) != 2:
    print("Usage: python transformer.py path/to/dataset.csv")
    sys.exit(1)

# Define file paths for input CSV and output Turtle file
input_file_path = sys.argv[1]
output_file_path = "graph.ttl"

# Load the dataset, parse dates in the index
df = pd.read_csv(input_file_path, index_col=0, parse_dates=True)

# Drop the 'cet_cest_timestamp' column as it's not needed for the RDF graph
df.drop(columns=['cet_cest_timestamp'], inplace=True)

# Initialize the RDF graph
rdf_graph = Graph()

# Define namespaces
SAREF = Namespace("https://saref.etsi.org/core/")
EX = Namespace("https://jordi.com/")

# Bind namespaces to prefixes for easier use
rdf_graph.bind("saref", SAREF)
rdf_graph.bind("ex", EX)

# Function to split the device column names into building and device type
def split_device_column(column_name):
    parts = column_name.split('_')
    building = '_'.join(parts[:3])  # First three parts form the building identifier
    device_type = '_'.join(parts[3:])  # Remaining parts form the device type
    return building, device_type

# Iterate over each row in the dataframe
for timestamp, row in df.iterrows():
    # Create a URI for each timestamp
    timestamp_uri = URIRef(f"https://jordi.com/timestamp{timestamp.isoformat()}")

    # Add the triples for each measurement in the row
    for device, value in row.items():
        # Split the device column into building and device type
        building, device_type = split_device_column(device)
        
        # Check if the value is not NaN and is either an integer or a float
        if pd.notna(value) and isinstance(value, (int, float)):
            # Create URIs for the measurement, device, and building
            measurement_uri = URIRef(f"https://jordi.com/measurement/{building}/{device_type}/{timestamp.isoformat()}")
            device_uri = URIRef(f"https://jordi.com/device/{building}/{device_type}")
            building_uri = URIRef(f"https://jordi.com/building/{building}")

            # Add triples to the graph
            rdf_graph.add((measurement_uri, RDF.type, SAREF.Measurement))  # Declare the measurement as an instance of SAREF.Measurement
            rdf_graph.add((measurement_uri, SAREF.hasTimestamp, Literal(timestamp.isoformat(), datatype=XSD.dateTime)))  # Add timestamp to the measurement
            rdf_graph.add((measurement_uri, SAREF.hasValue, Literal(float(value), datatype=XSD.float)))  # Add value to the measurement
            rdf_graph.add((measurement_uri, SAREF.isMeasuredIn, SAREF.KiloWattHour))  # Specify the unit of measurement (kWh) taken from excel as kWh
            rdf_graph.add((measurement_uri, SAREF.isMeasuredByDevice, EX[device_type]))  # Link the measurement to the type of device
            rdf_graph.add((measurement_uri, SAREF.isLocatedIn, EX[building]))  # Link the measurement to the building

            rdf_graph.add((device_uri, RDF.type, SAREF.Device))  # Declare the device as an instance of SAREF.Device
            rdf_graph.add((device_uri, SAREF.hasModel, Literal(device_type, datatype=XSD.string)))  # Add model information to the device
            rdf_graph.add((device_uri, SAREF.isLocatedIn, EX[building]))  # Link the device to the building

            rdf_graph.add((EX[building], RDF.type, SAREF.Building))  # Declare the building as an instance of SAREF.Building

# Serialize the graph in Turtle format
turtle_data = rdf_graph.serialize(format='turtle')

# Write the Turtle data to a file
with open(output_file_path, "w", encoding="utf-8") as output_file:
    output_file.write(turtle_data)  # Save the Turtle data to a file

print(f"RDF graph has been serialized and saved as {output_file_path}")  # Print a confirmation message
